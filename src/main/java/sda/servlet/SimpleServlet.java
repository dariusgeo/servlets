package sda.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SimpleServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	private String name = null;
	private String email = null;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		name = config.getInitParameter("name");
		email = config.getInitParameter("email");
	}
	
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		super.service(req, resp);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {	
		handleRequest(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {		
		handleRequest(req, resp);
	}

	@Override
	public void destroy() {
		// Close opened resources: DB connections, streams, etc..
	}
		
	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) {
		
		String param1 = req.getParameter("param1");
		
		try {
			PrintWriter out = resp.getWriter();
			out.println("<html>");
			out.println("<body>");
			out.println("<h1>Init params</h1>");
			out.println("<b>" + name + "</b>");			
			out.println("<b>" + email + "</b>");
			out.println("<h1>GET params</h1>");
			out.println("<b>" + param1 + "</b>");
			out.println("</body>");
			out.println("</html>");	
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		
	}

}